# Règlements intérieurs et annexes de la DG
Ce dépôt contient les versions officielles (ayant force de loi) du règlement
intérieur, de ses annexes et de ses traductions.

## Récupérer et modifier les textes dans Wagtail
Wagtail (CMS du site de la DG) peut conserver en mémoire différentes versions
d’une même page, mais celles-ci ne sont accessibles qu’aux administrateurs, et
il ne semble pas exister de fonctionnalité de *diff*. Par ailleurs, il est
difficile de modifier les pages en texte brut, seul le WYSIWYG est accessible
dans l’interface de modification.

Pour accéder au text brut des pages de Wagtail, il est nécessaire de récupérer
les objets de type `sitedg.models.TranslatedPage`, la source HTML des pages
étant disponible dans les champs `body` (pour le français) et `body_en` (pour
l’anglais). Les `TranslatedPage`s sont identifiées par un `slug` (fin de leur
URL), ce qui permet de les filtrer facilement.

Le modèle est aussi enregistré `TranslatedPage` dans `django-admin`.

## Conventions
Merci de maintenir une source HTML **propre** (correctement indentée, valide,
avec des lignes d’une largeur raisonnable).
